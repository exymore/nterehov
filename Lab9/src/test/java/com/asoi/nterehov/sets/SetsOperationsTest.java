package com.asoi.nterehov.sets;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

public class SetsOperationsTest {

    @Test
    public void testUnion() {
        HashSet<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));
        HashSet<Integer> set2 = new HashSet<>(Arrays.asList(4, 5, 6, 7, 8));

        HashSet<Integer> expected = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
        HashSet<Integer> actual = SetsOperations.union(set1,set2);

        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testIntersection() {
        HashSet<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));
        HashSet<Integer> set2 = new HashSet<>(Arrays.asList(4, 5, 6, 7, 8));

        HashSet<Integer> expected = new HashSet<>(Arrays.asList(4, 5));
        HashSet<Integer> actual = SetsOperations.intersection(set1,set2);

        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testDifference() {
        HashSet<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));
        HashSet<Integer> set2 = new HashSet<>(Arrays.asList(4, 5, 6, 7, 8));

        HashSet<Integer> expected = new HashSet<>(Arrays.asList(1, 2, 3, 6, 7, 8));
        HashSet<Integer> actual = SetsOperations.difference(set1,set2);

        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testSubtraction() {
        HashSet<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));
        HashSet<Integer> set2 = new HashSet<>(Arrays.asList(4, 5, 6, 7, 8));

        HashSet<Integer> expected = new HashSet<>(Arrays.asList(1, 2, 3));
        HashSet<Integer> actual = SetsOperations.subtraction(set1,set2);

        Assert.assertEquals(expected,actual);
    }
}