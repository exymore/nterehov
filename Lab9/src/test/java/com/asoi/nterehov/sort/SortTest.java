package com.asoi.nterehov.sort;

import org.junit.Assert;
import org.junit.Test;

public class SortTest {

    @Test
    public void defSort() {
        Integer[] arr = {11, 55, 24, 34, 78, 999, 34, 100, 16};
        Sort.sort(arr);
        Integer[] expected = {100, 11, 24, 34, 34, 16, 55, 78, 999};
        Assert.assertArrayEquals(expected, arr);
    }

    @Test
    public void emptySort() {
        Integer[] arr = {};
        Sort.sort(arr);
        Assert.assertEquals(new Integer[0], arr);
    }
}
