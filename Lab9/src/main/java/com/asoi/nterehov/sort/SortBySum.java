package com.asoi.nterehov.sort;

import java.util.Comparator;

class SortBySum implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        return Integer.compare(sumOfDigits(o1), sumOfDigits(o2));
    }

    private static int sumOfDigits(Integer num) {
        int sum = 0;
        while (num > 0) {
            sum += num % 10;
            num = num / 10;
        }
        return sum;
    }
}
