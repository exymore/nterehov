package com.asoi.nterehov.sort;

import java.util.Arrays;

public class Sort {

    public static void sort(Integer[] arr) {
        Arrays.sort(arr, new SortBySum());
    }
}
