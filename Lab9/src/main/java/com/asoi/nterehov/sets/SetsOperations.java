package com.asoi.nterehov.sets;

import java.util.HashSet;

public class SetsOperations {

    static <T> HashSet<T> union(HashSet<T> s1, HashSet<T> s2) {

        for (T item : s1) {
            if (!isExists(item, s2))
                s2.add(item);
        }
        return s2;
    }

    static <T> HashSet<T> intersection(HashSet<T> s1, HashSet<T> s2) {

        HashSet<T> result = new HashSet<>();
        for (T item : s1) {
            if (isExists(item, s2))
                result.add(item);
        }
        return result;
    }

    static <T> HashSet<T> difference(HashSet<T> s1, HashSet<T> s2) {

        HashSet<T> result = new HashSet<>();

        for (T item : s1) {
            if (!isExists(item, s2))
                result.add(item);
        }

        for (T item : s2) {
            if (!isExists(item, s1))
                result.add(item);
        }
        return result;
    }

    static <T> HashSet<T> subtraction(HashSet<T> s1, HashSet<T> s2) {

        for (T item : s1) {
            if (isExists(item, s2))
                s1.remove(item);
        }
        return s1;
    }

    private static <T> boolean isExists(T e, HashSet<T> s) {
        return s.contains(e);
    }
}
