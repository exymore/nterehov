package com.asoi.nterehov.mylist;

public class MyListIsEmptyException extends Exception {
    public MyListIsEmptyException(String message) {
        super(message);
    }
}
