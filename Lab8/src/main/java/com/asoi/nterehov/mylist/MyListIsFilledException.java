package com.asoi.nterehov.mylist;

public class MyListIsFilledException extends Exception {
    public MyListIsFilledException(String message) {
        super(message);
    }
}
