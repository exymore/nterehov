package com.asoi.nterehov.mylist;

import java.util.Arrays;
import java.util.Collection;

public class MyList<E> {

    private int size = 0;

    private static final int DEFAULT_CAPACITY = 5;
    private static final int MAX_CAPACITY = 10;

    private Object[] elements;

    MyList() {
        elements = new Object[DEFAULT_CAPACITY];
        size = DEFAULT_CAPACITY;
    }

    MyList(Collection<? extends E> c) throws MyListIsFilledException, MyListIsEmptyException {

        var objects = c.toArray();

        if (objects.length > MAX_CAPACITY) throw new
                MyListIsFilledException("You cannot create a list with more than 10 capacity");

        if (objects.length != 0) {
            elements = Arrays.copyOf(objects, objects.length, Object[].class);
            size = elements.length;
        } else
            throw new MyListIsEmptyException("List is empty!");
    }

    MyList(int initialCapacity) throws MyListIsFilledException, MyListIsEmptyException {
        if (initialCapacity > MAX_CAPACITY) throw new
                MyListIsFilledException("You cannot create a list with more than 10 capacity");
        if (initialCapacity != 0) {
            elements = new Object[initialCapacity];
            size = initialCapacity;
        }
        else {
            throw new MyListIsEmptyException("List is empty!");
        }
    }

    void add(E e) throws MyListIsFilledException{
        if (size == MAX_CAPACITY) {
            throw new MyListIsFilledException("List is filled already");
        }
        elements[size++] = e;
    }

    int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (elements.length != 0) {
            for (Object element : elements) {
                sb.append(element + " ");
            }
            sb.deleteCharAt(sb.length() - 1);
            return sb.toString();
        }
        else
            return null;
    }
}
