package com.asoi.nterehov.builders;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

class ContractMatch {


    static int isMatch(ArrayList<Builder> brigade) {
        TreeMap<Skills, Integer> contractCopy = Contract.getContract();

        TreeMap<Skills, Integer> contract = (TreeMap<Skills, Integer>) contractCopy.clone();

        int totalCost = 0;

        for (Map.Entry<Skills, Integer> requirement : contract.entrySet()) {
            for (Builder builder: brigade) {
                if (builder.getSkills().contains(requirement.getKey())) {
                    requirement.setValue(requirement.getValue() - 1);
                }
            }
        }

        for (Map.Entry<Skills, Integer> requirement : contract.entrySet()) {
            if (requirement.getValue() > 0) {
                return 0;
            }
        }

        for (Builder builder: brigade) {
            totalCost += builder.getSalary();
        }
        return totalCost;

    }
}
