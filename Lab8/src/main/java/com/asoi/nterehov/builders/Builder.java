package com.asoi.nterehov.builders;

import java.util.ArrayList;

public class Builder {

    private int salary;
    private ArrayList<Skills> skills;

    Builder(int salary, ArrayList<Skills> skills) throws BuilderException{
        if (salary <= 0)
            throw new BuilderException("Salary cannot be less than or equal to zero!");
        else
            this.salary = salary;

        this.skills = skills;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public ArrayList<Skills> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<Skills> skills) {
        this.skills = skills;
    }
}
