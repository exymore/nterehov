package com.asoi.nterehov.builders;

class BuilderException extends Exception {
    BuilderException(String message) {
        super(message);
    }
}
