package com.asoi.nterehov.builders;

public enum Skills {
    ENGINEERING,
    HIGH_SPEED,
    ATHLETIC,
    YOUNG,
    SKILLFUL
}
