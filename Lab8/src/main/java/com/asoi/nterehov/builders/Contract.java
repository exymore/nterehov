package com.asoi.nterehov.builders;

import java.util.TreeMap;

final class Contract {

    private int BUILDERS_COUNT = 5;
    private static TreeMap<Skills, Integer> requirements = new TreeMap<>();

    private Contract() {
        requirements.put(Skills.SKILLFUL, 2);
        requirements.put(Skills.YOUNG, 2);
        requirements.put(Skills.ENGINEERING, 2);
    }

    static TreeMap<Skills, Integer> getContract() {
        if (requirements.size() == 0) {
            new Contract();
        }
        return requirements;
    }
}