package com.asoi.nterehov.builders;

public class Match {

    static void match(int brigade1Cost, int brigade2Cost) throws BuilderException {
        if (brigade1Cost == 0) {
            if (brigade2Cost == 0)
                throw new BuilderException("Neither brigade satisfies the requirements!");
            else
                System.out.println("Brigade 2 satisfies the requirements!");
        }
        else
        if (brigade1Cost < brigade2Cost)
            System.out.println("Brigade 1 satisfies the requirements!");
        else if (brigade1Cost > brigade2Cost)
            System.out.println("Brigade 2 satisfies the requirements!");
        else
            System.out.println("Both brigades cost are equals and satisfies the requirements!");
    }
}
