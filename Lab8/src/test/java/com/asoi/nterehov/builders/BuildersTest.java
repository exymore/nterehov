package com.asoi.nterehov.builders;

import org.junit.Test;

import java.util.ArrayList;

import static java.util.Arrays.asList;

public class BuildersTest {

    @Test
    public void firstBrigadeSatisfies() throws BuilderException {
        Builder b1 = new Builder(1000, new ArrayList<>(asList(Skills.ATHLETIC)) {
        });
        Builder b2 = new Builder(2000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.SKILLFUL)) {
        });
        Builder b3 = new Builder(3000, new ArrayList<>(asList(Skills.HIGH_SPEED, Skills.SKILLFUL, Skills.ATHLETIC)) {
        });
        Builder b4 = new Builder(4000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.YOUNG, Skills.SKILLFUL, Skills.ATHLETIC)) {
        });
        Builder b5 = new Builder(5000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.SKILLFUL, Skills.ATHLETIC, Skills.HIGH_SPEED, Skills.YOUNG)) {
        });

        Builder b6 = new Builder(2000, new ArrayList<>(asList(Skills.YOUNG)) {
        });
        Builder b7 = new Builder(3000, new ArrayList<>(asList(Skills.YOUNG, Skills.SKILLFUL)) {
        });
        Builder b8 = new Builder(4000, new ArrayList<>(asList(Skills.YOUNG, Skills.SKILLFUL, Skills.HIGH_SPEED)) {
        });
        Builder b9 = new Builder(5000, new ArrayList<>(asList(Skills.YOUNG, Skills.SKILLFUL, Skills.HIGH_SPEED, Skills.ENGINEERING)) {
        });
        Builder b10 = new Builder(6000, new ArrayList<>(asList(Skills.YOUNG, Skills.SKILLFUL, Skills.HIGH_SPEED, Skills.ENGINEERING, Skills.ATHLETIC)) {
        });

        ArrayList<Builder> brigade1 = new ArrayList<>(asList(b1, b2, b3, b4, b5));
        ArrayList<Builder> brigade2 = new ArrayList<>(asList(b6, b7, b8, b9, b10));

        int brigade1Cost = ContractMatch.isMatch(brigade1);
        int brigade2Cost = ContractMatch.isMatch(brigade2);

        Match.match(brigade1Cost, brigade2Cost);
    }

    @Test
    public void secondBrigadeSatisfies() throws BuilderException {
        Builder b1 = new Builder(1000, new ArrayList<>(asList(Skills.ATHLETIC)) {
        });
        Builder b2 = new Builder(2000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.SKILLFUL)) {
        });
        Builder b3 = new Builder(3000, new ArrayList<>(asList(Skills.HIGH_SPEED, Skills.SKILLFUL, Skills.ATHLETIC)) {
        });
        Builder b4 = new Builder(4000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.YOUNG, Skills.SKILLFUL, Skills.ATHLETIC)) {
        });
        Builder b5 = new Builder(5000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.SKILLFUL, Skills.ATHLETIC, Skills.HIGH_SPEED, Skills.YOUNG)) {
        });

        Builder b6 = new Builder(200, new ArrayList<>(asList(Skills.YOUNG)) {
        });
        Builder b7 = new Builder(300, new ArrayList<>(asList(Skills.YOUNG, Skills.SKILLFUL)) {
        });
        Builder b8 = new Builder(400, new ArrayList<>(asList(Skills.YOUNG, Skills.SKILLFUL, Skills.HIGH_SPEED)) {
        });
        Builder b9 = new Builder(500, new ArrayList<>(asList(Skills.YOUNG, Skills.SKILLFUL, Skills.HIGH_SPEED, Skills.ENGINEERING)) {
        });
        Builder b10 = new Builder(600, new ArrayList<>(asList(Skills.YOUNG, Skills.SKILLFUL, Skills.HIGH_SPEED, Skills.ENGINEERING, Skills.ATHLETIC)) {
        });

        ArrayList<Builder> brigade1 = new ArrayList<>(asList(b1, b2, b3, b4, b5));
        ArrayList<Builder> brigade2 = new ArrayList<>(asList(b6, b7, b8, b9, b10));

        int brigade1Cost = ContractMatch.isMatch(brigade1);
        int brigade2Cost = ContractMatch.isMatch(brigade2);

        Match.match(brigade1Cost, brigade2Cost);
    }

    @Test(expected = BuilderException.class)
    public void neitherBrigadeSatisfies() throws BuilderException {
        Builder b1 = new Builder(1000, new ArrayList<>(asList(Skills.ATHLETIC)) {
        });
        Builder b2 = new Builder(2000, new ArrayList<>(asList(Skills.ENGINEERING)) {
        });
        Builder b3 = new Builder(3000, new ArrayList<>(asList(Skills.HIGH_SPEED, Skills.ATHLETIC)) {
        });
        Builder b4 = new Builder(4000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.YOUNG, Skills.ATHLETIC)) {
        });
        Builder b5 = new Builder(5000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.ATHLETIC, Skills.HIGH_SPEED, Skills.YOUNG)) {
        });

        Builder b6 = new Builder(2000, new ArrayList<>(asList(Skills.YOUNG)) {
        });
        Builder b7 = new Builder(3000, new ArrayList<>(asList(Skills.SKILLFUL)) {
        });
        Builder b8 = new Builder(4000, new ArrayList<>(asList(Skills.SKILLFUL, Skills.HIGH_SPEED)) {
        });
        Builder b9 = new Builder(5000, new ArrayList<>(asList(Skills.SKILLFUL, Skills.HIGH_SPEED, Skills.ENGINEERING)) {
        });
        Builder b10 = new Builder(6000, new ArrayList<>(asList(Skills.SKILLFUL, Skills.HIGH_SPEED, Skills.ENGINEERING, Skills.ATHLETIC)) {
        });

        ArrayList<Builder> brigade1 = new ArrayList<>(asList(b1, b2, b3, b4, b5));
        ArrayList<Builder> brigade2 = new ArrayList<>(asList(b6, b7, b8, b9, b10));

        int brigade1Cost = ContractMatch.isMatch(brigade1);
        int brigade2Cost = ContractMatch.isMatch(brigade2);

        Match.match(brigade1Cost, brigade2Cost);
    }

    @Test
    public void bothBrigadesSatisfies() throws BuilderException {
        Builder b1 = new Builder(1000, new ArrayList<>(asList(Skills.ATHLETIC)) {
        });
        Builder b2 = new Builder(2000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.SKILLFUL)) {
        });
        Builder b3 = new Builder(3000, new ArrayList<>(asList(Skills.HIGH_SPEED, Skills.SKILLFUL, Skills.ATHLETIC)) {
        });
        Builder b4 = new Builder(4000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.YOUNG, Skills.SKILLFUL, Skills.ATHLETIC)) {
        });
        Builder b5 = new Builder(5000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.SKILLFUL, Skills.ATHLETIC, Skills.HIGH_SPEED, Skills.YOUNG)) {
        });

        Builder b6 = new Builder(1000, new ArrayList<>(asList(Skills.ATHLETIC)) {
        });
        Builder b7 = new Builder(2000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.SKILLFUL)) {
        });
        Builder b8 = new Builder(3000, new ArrayList<>(asList(Skills.HIGH_SPEED, Skills.SKILLFUL, Skills.ATHLETIC)) {
        });
        Builder b9 = new Builder(4000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.YOUNG, Skills.SKILLFUL, Skills.ATHLETIC)) {
        });
        Builder b10 = new Builder(5000, new ArrayList<>(asList(Skills.ENGINEERING, Skills.SKILLFUL, Skills.ATHLETIC, Skills.HIGH_SPEED, Skills.YOUNG)) {
        });

        ArrayList<Builder> brigade1 = new ArrayList<>(asList(b1, b2, b3, b4, b5));
        ArrayList<Builder> brigade2 = new ArrayList<>(asList(b6, b7, b8, b9, b10));

        int brigade1Cost = ContractMatch.isMatch(brigade1);
        int brigade2Cost = ContractMatch.isMatch(brigade2);

        Match.match(brigade1Cost, brigade2Cost);
    }

}
