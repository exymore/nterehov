package com.asoi.nterehov.mylist;

import org.junit.Test;

import java.util.Arrays;

public class MyListTest {
    @Test
    public void defaultInit() {
        MyList<Integer> integerMyList = new MyList<>();
        MyList<String> stringMyList = new MyList<>();
        System.out.println(integerMyList.toString());
        System.out.println(stringMyList.toString());
    }

    @Test
    public void collectionInit() throws MyListIsFilledException, MyListIsEmptyException {
        MyList<Integer> integerMyList = new MyList<>(Arrays.asList(1, 2, 3, 4, 5));
        MyList<String> stringMyList = new MyList<>(Arrays.asList("1", "2", "3", "4", "5"));
        System.out.println(integerMyList.toString());
        System.out.println(stringMyList.toString());
    }

    @Test
    public void initialCapacityInit() throws MyListIsFilledException, MyListIsEmptyException{
        MyList<Integer> integerMyList = new MyList<>(4);
        MyList<String> stringMyList = new MyList<>(4);
        System.out.println(integerMyList.toString());
        System.out.println(stringMyList.toString());
    }

    @Test (expected = MyListIsEmptyException.class)
    public void emptyInitialCapacityInit() throws MyListIsFilledException, MyListIsEmptyException {
        MyList<Integer> integerMyList = new MyList<>(0);
        MyList<String> stringMyList = new MyList<>(0);
        System.out.println(integerMyList.toString());
        System.out.println(stringMyList.toString());
    }

    @Test (expected = NegativeArraySizeException.class)
    public void negativeInitialCapacityInit() throws MyListIsFilledException, MyListIsEmptyException {
        MyList<Integer> integerMyList = new MyList<>(-1);
        MyList<String> stringMyList = new MyList<>(-1);
    }

    @Test (expected = MyListIsFilledException.class)
    public void addToEnd() throws MyListIsFilledException, MyListIsEmptyException {
        MyList<Integer> integerMyList = new MyList<>(10);
        MyList<String> stringMyList = new MyList<>(10);
        integerMyList.add(11);
        stringMyList.add("11");
    }

    @Test (expected = MyListIsFilledException.class)
    public void createListWithMoreThanMaxCapacityByInitial() throws MyListIsFilledException, MyListIsEmptyException {
        MyList<Integer> integerMyList = new MyList<>(11);
    }

    @Test (expected = MyListIsFilledException.class)
    public void createListWithMoreThanMaxCapacityByCollection() throws MyListIsFilledException, MyListIsEmptyException {
        MyList<Integer> integerMyList = new MyList<>
                (Arrays.asList(1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5));
    }
}
