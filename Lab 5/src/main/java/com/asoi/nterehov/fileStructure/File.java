package com.asoi.nterehov.fileStructure;

import java.util.ArrayList;

public class File extends Component{

    public File(String name) {
        super(name);
    }

    private ArrayList<Component> files = new ArrayList<Component>();

    @Override
    public void add(Component component) {
        files.add(component);
    }

}
