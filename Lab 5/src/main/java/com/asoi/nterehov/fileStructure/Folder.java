package com.asoi.nterehov.fileStructure;

import java.util.ArrayList;

public class Folder extends Component {

    public Folder(String name) {
        super(name);
    }

    private ArrayList<Component> folders = new ArrayList<Component>();

    @Override
    public void add(Component component) {
        folders.add(component);
    }

    public void display() {
        System.out.printf(name + "/\n");
        for (int i = 0; i < folders.size(); i++) {
            folders.get(i).display();
        }
    }

}
