package com.asoi.nterehov.fileStructure;

public class Client {

    public static void main (String[] args) {

        Component root = new Folder("root");
        Component folder1 = new Folder("folder1");
        Component file1 = new File("file1");

        Component folder2 = new Folder("folder2");
        Component file2 = new File("file2");

        root.add(folder1);
        folder1.add(file1);

        root.add(folder2);
        folder2.add(file2);

        root.display();
    }
}
