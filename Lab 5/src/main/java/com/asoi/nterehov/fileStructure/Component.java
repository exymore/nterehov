package com.asoi.nterehov.fileStructure;

abstract class Component {

    protected String name;

    public Component (String name) {
        this.name = name;
    }

    public void add(Component component) {}

    public void display() {
        System.out.printf("\t" + name + "\n");
    }

}
