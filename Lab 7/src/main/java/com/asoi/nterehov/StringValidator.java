package com.asoi.nterehov;

public class StringValidator implements Validator<String> {

    @Override
    public boolean validate(String data) {

        if (data.length() != 0) {
            int firstLetter = (int) data.charAt(0);
            return firstLetter >= 65 && firstLetter <= 90;
        } else return false;
    }
}
