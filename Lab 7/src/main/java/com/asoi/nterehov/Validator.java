package com.asoi.nterehov;

interface Validator<T> {
    boolean validate(T data);
}
