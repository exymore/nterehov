package com.asoi.nterehov;

public class ValidationSystem<T> {

    private T data;

    public void main(String[] args) {
        try {
            validate(data);
        } catch (ValidationFailedException e) {
            System.out.println(e.getMessage());
        }
    }

    static <T> void validate(T data) throws ValidationFailedException {
        Validator validator;

        if (data.getClass().equals(Integer.class)) {
            validator = new IntegerValidator();
            if (validator.validate(data))
                System.out.println("This is a valid Integer");
            else
                throw new ValidationFailedException();
        } else if (data.getClass().equals(String.class)) {
            validator = new StringValidator();
            if (validator.validate(data))
                System.out.println("This is a valid String");
            else
                throw new ValidationFailedException();
        }
    }
}
