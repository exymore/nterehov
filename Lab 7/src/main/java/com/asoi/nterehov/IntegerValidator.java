package com.asoi.nterehov;

public class IntegerValidator implements Validator<Integer> {

    @Override
    public boolean validate(Integer data) {
        return data >= 1 && data <= 10;
    }
}
