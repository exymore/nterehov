package com.asoi.nterehov;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Grouping {
    public static ArrayList<Character> fillCharset(TreeMap<String, Integer> words) {
        ArrayList<Character> charset = new ArrayList<>();

        if (words.size() != 0) {
            for (Map.Entry<String, Integer> item : words.entrySet()) {
                if (!charset.contains(item.getKey().charAt(0)))
                    charset.add(item.getKey().charAt(0));
            }
        }
        else { System.out.println("Input string is empty!"); }
        return charset;
    }
}
