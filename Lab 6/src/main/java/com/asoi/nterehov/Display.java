package com.asoi.nterehov;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Display {
    public static void display(TreeMap<String, Integer> words, ArrayList<Character> charset) {

        int current = 0;
        if (charset.size() != 0) {
            System.out.println(Character.toUpperCase(charset.get(current)) + ":");
            for (Map.Entry<String, Integer> item : words.entrySet()) {
                if (item.getKey().charAt(0) == charset.get(current)) {
                    System.out.println("  " + item.getKey() + ' ' + item.getValue());
                } else {
                    System.out.println(Character.toUpperCase(charset.get(++current)) + ":\n  " + item.getKey() + " " + item.getValue());
                }
            }
        }
    }
}
