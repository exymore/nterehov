package com.asoi.nterehov;

import java.util.ArrayList;
import java.util.TreeMap;

public class Main {

    public static void main(String args[]) {

        String input = "ONCE upon a time a Wolf was lapping at a spring on a hillside when, looking up, what should " +
                "he see but a Lamb just beginning to drink a little lower down.";

        TreeMap<String, Integer> words = Words.splitWords(input);
        ArrayList<Character> charset = Grouping.fillCharset(words);
        Display.display(words, charset);
    }
}
