package com.asoi.nterehov;

import java.util.TreeMap;

public class Words {

    public static TreeMap<String, Integer> splitWords(String input) {

        String[] raw_words = input.toLowerCase().split("[,;:.!?\\s]+");

        TreeMap<String, Integer> words = new TreeMap<>();

        for (String word : raw_words) {
            if (!words.containsKey(word) && !word.equals("")) {
                words.put(word, 1);
            } else if (words.containsKey(word)) {
                int count = words.get(word);
                count++;
                words.put(word, count);
            }
        }
        return words;
    }
}
