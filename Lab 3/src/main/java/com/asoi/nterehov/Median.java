package com.asoi.nterehov;

import java.util.Arrays;

public class Median {

    static float median(int[] array)
    {
        int[] nums = array;
        Arrays.sort(nums);
        int length = nums.length;

        if (length % 2 != 0) {
            return nums[(length - 1) / 2];
        }

        else if (length % 2 == 0 && length != 0) {
            float v1 = nums[length / 2];
            float v2 = nums[length / 2 - 1];
            float result = (v1 + v2) / 2;
            return result;
        }
        else return 0;
    }

    static double median(double[] nums)
    {
        Arrays.sort(nums);
        int length = nums.length;

        if (nums.length % 2 != 0) {
            return nums[(nums.length - 1) / 2];
        }

        else if (length % 2 == 0 && length != 0) {
            double v1 = nums[length / 2];
            double v2 = nums[length / 2 - 1];
            double result = (v1 + v2) / 2;
            return result;
        }
        else return 0;
    }
}