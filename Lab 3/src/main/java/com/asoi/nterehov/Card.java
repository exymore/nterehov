package com.asoi.nterehov;

public class Card {

    private String ownerName;
    private int balance;

    {
        balance = 0;
    }

    Card(String ownerName) {
        this.ownerName = ownerName;
    }

    Card(String ownerName, int balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    int get() {
        return this.balance;
    }

    void deposit(int cash) {
        this.balance += cash;
    }

    void withdraw(int cash) {
        this.balance -= cash;
    }

    float converter(int cash, float multiplier) {
        return cash * multiplier;
    }
}