package com.asoi.nterehov;

import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    private Card card = new Card("Owner", 25200);

    @Test
    public void get() {
        int actual = card.get();
        assertEquals(25200, actual);
    }

    @Test
    public void deposit() {
        card.deposit(800);
        int actual = card.get();
        assertEquals(26000, actual);
    }

    @Test
    public void withdraw() {
        card.withdraw(200);
        int actual = card.get();
        assertEquals(25000, actual);
    }

    @Test
    public void converter() {
        float actual = card.converter(card.get(), 10);
        assertEquals(2520, actual, 0);
    }
}