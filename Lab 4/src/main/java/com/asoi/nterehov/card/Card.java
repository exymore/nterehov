package com.asoi.nterehov.card;

public class Card {

    public String ownerName;
    public int balance;

    {
        balance = 0;
    }

    public Card(String ownerName, int balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public int getBalance() {
        return this.balance;
    }

    public void deposit(int cash) {
        this.balance += cash;
    }

    public void withdraw(int cash) {
        this.balance -= cash;
    }

    public float converter(int cash, float multiplier) {
        return cash * multiplier;
    }
}