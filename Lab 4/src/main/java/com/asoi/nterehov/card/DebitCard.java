package com.asoi.nterehov.card;

public class DebitCard extends Card {

    public DebitCard(String ownerName, int balance) {
        super(ownerName, balance);
    }

    @Override
    public void withdraw(int cash) {
        if (this.balance - cash >= 0)
            this.balance -= cash;
        else
            System.out.printf("Недостаточно средств!");
    }
}
