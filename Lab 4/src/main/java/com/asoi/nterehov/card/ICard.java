package com.asoi.nterehov.card;

public interface ICard {
    void deposit(int cash);
    void withdraw(int cash);
}
