package com.asoi.nterehov.atm;

import com.asoi.nterehov.card.DebitCard;
import com.asoi.nterehov.card.ICard;

public class ATMDebit implements ICard {

    public DebitCard card;

    public ATMDebit(DebitCard card) {
        this.card = card;
    }

    public void deposit(int cash) {
        card.deposit(cash);
    }

    public void withdraw(int cash) {
        card.withdraw(cash);
    }
}
