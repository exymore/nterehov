package com.asoi.nterehov.atm;

import com.asoi.nterehov.card.CCard;
import com.asoi.nterehov.card.ICard;

public class ATMCredit implements ICard {

    public CCard card;

    public ATMCredit(CCard card) {
        this.card = card;
    }

    public void deposit(int cash) {
        card.deposit(cash);
    }

    public void withdraw(int cash) {
        card.withdraw(cash);
    }
}