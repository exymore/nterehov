package com.asoi.nterehov.sort;

public class Context {

    public ISorter sortStrategy;

    public Context(ISorter strategy) {
        sortStrategy = strategy;
    }

    public void execute(int[] array) {
        sortStrategy.Sort(array);
    }
}
