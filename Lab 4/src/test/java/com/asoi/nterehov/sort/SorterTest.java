package com.asoi.nterehov.sort;

import com.asoi.nterehov.sort.BubbleSort;
import com.asoi.nterehov.sort.SelectionSort;
import org.junit.Assert;
import org.junit.Test;

// Я не знаю, почему тесты не работают, но я пытался
// assertArrayEquals не работает, поэтому здесь .toString()

public class SorterTest {

    int[] array = new int[] { 5, 4, 2, 3, 6, 1 };
    Context bubble = new Context(new BubbleSort());
    Context select = new Context(new SelectionSort());

    @Test
    public void BubbleTest() {
        bubble.execute(array);
        int[] expected = new int[] { 1, 2, 3, 4, 5, 6 };
        int[] actual = array;
        Assert.assertEquals(
                (expected.toString()),
                (actual.toString()));
    }

    @Test
    public void SelectionTest() {
        select.execute(array);
        int[] expected = new int[] { 1, 2, 3, 4, 5, 6 };
        int[] actual = array;
        Assert.assertEquals(
                (expected.toString()),
                (actual.toString()));
    }
}
