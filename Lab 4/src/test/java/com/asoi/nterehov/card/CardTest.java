package com.asoi.nterehov.card;

import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    private Card card = new Card("Owner", 25200);

    @Test
    public void get() {
        int actual = card.getBalance();
        assertEquals(25200, actual);
    }

    @Test
    public void deposit() {
        card.deposit(800);
        int actual = card.getBalance();
        assertEquals(26000, actual);
    }

    @Test
    public void withdraw() {
        card.withdraw(200);
        int actual = card.getBalance();
        assertEquals(25000, actual);
    }

    @Test
    public void converter() {
        float actual = card.converter(card.getBalance(), 10);
        assertEquals(2520, actual, 0);
    }
}