package com.asoi.nterehov.atm;

import com.asoi.nterehov.atm.ATMCredit;
import com.asoi.nterehov.atm.ATMDebit;
import com.asoi.nterehov.card.CCard;
import com.asoi.nterehov.card.DebitCard;
import org.junit.Assert;
import org.junit.Test;

public class ATMTest {

    CCard creditCard = new CCard("nterehov", 250);
    DebitCard debitCard = new DebitCard("nterehov", 250);

    ATMCredit atmCredit = new ATMCredit(creditCard);
    ATMDebit atmDebit = new ATMDebit(debitCard);

    @Test
    public void TestCreditDeposit() {
        atmCredit.deposit(500);
        int result = atmCredit.card.getBalance();
        Assert.assertEquals(750, result);
    }

    @Test
    public void TestDebitDeposit() {
        atmDebit.deposit(500);
        int result = atmDebit.card.getBalance();
        Assert.assertEquals(750, result);
    }

    @Test
    public void TestCreditWithdraw() {
        atmCredit.withdraw(500);
        int result = atmCredit.card.getBalance();
        Assert.assertEquals(-250, result);
    }

    @Test
    public void TestDebitWithdraw() {
        atmDebit.withdraw(500);
        int result = atmDebit.card.getBalance();
        Assert.assertEquals(250, result);
    }
}