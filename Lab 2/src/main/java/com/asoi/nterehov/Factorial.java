package com.asoi.nterehov;

import java.util.InputMismatchException;
import java.util.Scanner;

class Factorial {

    Scanner in = new Scanner(System.in);
        System.out.println("При вычислении ряда Фибоначчи параметр не должен превышать 90, при вычислении факториала - 20");

        try {
        System.out.println("1: Фибоначчи\n2: Факториал");
        int algorithmID = in.nextInt();
        if (algorithmID != 1 && algorithmID != 2) { System.out.println("Неверно введён тип алгоритма"); return; }

        System.out.println("1: while\n2: do-while\n3: for");
        int loopType = in.nextInt();
        if (loopType != 1 && loopType != 2 && loopType != 3) { System.out.println("Неверно введён тип цикла"); return; }

        System.out.println("Введите аргумент");
        int n = in.nextInt();

        if (algorithmID == 1) {
            if (n > 90) { System.out.println("НЕ БОЛЬШЕ 90"); return; }
            else {
                long[] result = Fibonacci_sequence(loopType, n);
                System.out.println("Ряд Фибоначчи до " + n + " элемента");
                for (int i = 0; i < result.length - 1; i++) System.out.print(result[i] + " ");
            }
        }
        if (algorithmID == 2) {
            if (n <0 || n > 20) { System.out.println("Аргумент должен быть в пределах 20 > n > 0"); return; }
            System.out.println("Факториал числа " + n + " равен " + Factorial(loopType, n));
        }
        in.close();
    }
        catch (
    InputMismatchException ex) {
        System.out.print("Неверный ввод!");
    }

    private static long[] Fibonacci_sequence(int loopType, int arg)
    {
        if (arg == 0) {
            return new long[1]; }

        int i = 2;
        long[] result = new long[arg + 1];
        result[0] = 0; result[1] = 1;

        if (arg == 1) return result;

        switch (loopType) {
            case 1:
                while (i != arg) {
                    result[i] = result[i - 1] + result[i - 2];
                    i++;
                }
                break;
            case 2:
                do {
                    result[i] = result[i - 1] + result[i - 2];
                    i++;
                } while (i != arg);
                break;
            case 3:
                for (i = 2; i < arg; i++)
                {
                    result[i] = result[i - 1] + result[i - 2];
                }
                break;
        }
        return result;
    }

    private static long Factorial(int loopType, int arg) {
        if (arg == 0 || arg == 1) return 1;

        long i = 1;
        long result = 1;

        switch (loopType) {
            case 1:
                while (i <= arg) {
                    result = result * i;
                    i++;
                }
                break;
            case 2:
                do {
                    result = result * i;
                    i++;
                } while (i <= arg);
                break;
            case 3:
                for (i = 1; i <= arg; i++) {
                    result = result * i;
                }
                break;
        }
        return result;
    }
}