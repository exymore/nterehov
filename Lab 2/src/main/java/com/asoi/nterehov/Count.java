package com.asoi.nterehov;

import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Count {

    Scanner in = new Scanner(System.in);

        System.out.print("Введите 4 цифры");

        try {
            int a = in.nextInt();
            int p = in.nextInt();
            double m1 = in.nextInt();
            double m2 = in.nextInt();

            double result = 4 * Math.PI * (Math.pow(a, 3) / Math.pow(p, 2) * (m1 + m2));

            if (0 == p || m1 + m2 == 0) throw new ArithmeticException("Не стоит делить на ноль");

            DecimalFormat ft = new DecimalFormat("#.###");

            System.out.print("Результат: " + ft.format(result));
            in.close();
        }
        catch (ArithmeticException ex) {
            System.out.print(ex.getMessage());
        }
        catch (InputMismatchException ex) {
        System.out.print("Неверный ввод!");
        }
}